﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfTetris.Models;

namespace WpfTetris.ViewModels
{
    /// <summary>
    /// テトリスメイン
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            Field = new FieldViewModel(Model.Field);
            Model.Start();
        }

        /// <summary>
        ///  左移動
        /// </summary>
        public DelegateCommand MoveLeftCommand => new DelegateCommand(Model.MoveLeft);

        /// <summary>
        /// 右移動
        /// </summary>
        public DelegateCommand MoveRightCommand => new DelegateCommand(Model.MoveRight);

        /// <summary>
        /// 下移動
        /// </summary>
        public DelegateCommand MoveDownCommand => new DelegateCommand(Model.MoveDown);

        /// <summary>
        /// 左回転
        /// </summary>
        public DelegateCommand RotateLeftCommand => new DelegateCommand(Model.RotateLeft);

        /// <summary>
        /// 右回転
        /// </summary>
        public DelegateCommand RotateRightCommand => new DelegateCommand(Model.RotateRight);

        public MainModel Model { get; } = new MainModel();
        public FieldViewModel Field { get; }
    }
}
