﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfTetris.Models;

namespace WpfTetris.ViewModels
{
    /// <summary>
    /// テトリミノ積み上げる場所
    /// </summary>
    public class FieldViewModel : ViewModelBase
    {
        public FieldViewModel(FieldModel model)
        {
            SubscribePropertyChanged(model);
            Model = model;

            foreach (var row in model.CellMatrix)
            {
                CellMatrix.Add(row.Select(r => new CellViewModel(r)).ToList());
            }
        }

        /// <summary>
        /// モデル
        /// </summary>
        public FieldModel Model { get; }

        /// <summary>
        /// セル
        /// </summary>
        public IList<IList<CellViewModel>> CellMatrix { get; } = new List<IList<CellViewModel>>();


    }
}
