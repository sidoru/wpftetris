﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfTetris.Models;

namespace WpfTetris.ViewModels
{
    /// <summary>
    /// 画面（Field）上の1個のセル
    /// </summary>
    public class CellViewModel : ViewModelBase
    {
        /// <summary>
        /// ctor
        /// </summary>
        public CellViewModel(CellModel model)
        {
            SubscribePropertyChanged(model);
            Model = model;
        }

        /// <summary>
        /// モデル
        /// </summary>
        public CellModel Model { get; }

        /// <summary>
        /// 種別
        /// </summary>
        public CellType CellType => Model.CellType;
    }
}
