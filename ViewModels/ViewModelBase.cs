﻿using Prism.Mvvm;
using System.ComponentModel;
using System.Reflection;

namespace WpfTetris.ViewModels
{
    public class ViewModelBase : BindableBase
    {
        /// <summary>
        /// publisherの変更通知を自身の通知として再度投げる。
        /// </summary>
        /// <param name="publisher"></param>
        protected void SubscribePropertyChanged(INotifyPropertyChanged publisher)
        {
            publisher.PropertyChanged += LinkPropertyChanged;
        }

        private void LinkPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // サブクラスのプロパティも探す
            var type = GetType();
            while (type != typeof(ViewModelBase))
            {
                var typeInfo = type.GetTypeInfo();
                if (typeInfo.GetDeclaredProperty(e.PropertyName) != null)
                {
                    RaisePropertyChanged(e.PropertyName);
                    break;
                }
                else
                {
                    type = typeInfo.BaseType;
                }
            }
        }
    }
}
