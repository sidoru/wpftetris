﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTetris.Models
{
    public class CellModel : ModelBase
    {
        public CellModel(int x, int y)
        {
            Position = new Position(x, y);
        }

        public Position Position { get; }

        public CellType CellType { get; set; } = CellType.None;

        /// <summary>
        /// ToString
        /// </summary>
        public override string ToString()
        {
            return $"{Position} {CellType}";
        }
    }

    public enum CellType
    {
        // 何もない場所
        None,
        // 棒みたいなやつ
        I,
        // T
        T,
        // 四角
        O,
        // Lの反対
        J,
        // L
        L,
        // S
        S,
        // Z
        Z,
    }
}
