﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTetris.Models
{
    /// <summary>
    /// テトリミノファクトリ
    /// </summary>
    public class TetriminoFactory
    {
        /// <summary>
        /// 作る
        /// </summary>
        public TetriminoModel Create(TetriminoType type, Position position)
        {
            byte[,] shape;
            switch (type)
            {
                case TetriminoType.I:
                     shape =new byte[,]
                     {
                        { 1 },
                        { 1 },
                        { 1 },
                        { 1 },
                     };
                    break;
                case TetriminoType.T:
                    shape = new byte[,]
                    {
                        {1, 1, 1 },
                        {0, 1, 0 },
                    };
                    break;
                case TetriminoType.O:
                    shape = new byte[,]
                    {
                        {1, 1 },
                        {1, 1 },
                    };
                    break;
                case TetriminoType.J:
                    shape = new byte[,]
                    {
                        {0, 1 },
                        {0, 1 },
                        {1, 1 },
                    };
                    break;
                case TetriminoType.L:
                    shape = new byte[,]
                    {
                        {1, 0 },
                        {1, 0 },
                        {1, 1 },
                    };
                    break;
                case TetriminoType.S:
                    shape = new byte[,]
                    {
                        {1, 0,  },
                        {1, 1,  },
                        {0, 1,  },
                    };
                    break;
                case TetriminoType.Z:
                    shape = new byte[,]
                    {
                        {0, 1,  },
                        {1, 1,  },
                        {1, 0,  },
                    };
                    break;
                default:
                    throw new InvalidOperationException();
            };

            return new TetriminoModel(type, shape, position);
        }
    }

    public enum TetriminoType
    {
        // 棒みたいなやつ
        I,
        // T
        T,
        // 四角
        O,
        // Lの反対
        J,
        // L
        L,
        // S
        S,
        // Z
        Z
    }
}
