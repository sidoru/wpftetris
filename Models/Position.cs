﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTetris.Models
{
    /// <summary>
    /// 位置
    /// </summary>
    public struct Position
    {
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        /// <summary>
        /// ToString
        /// </summary>
        public override string ToString()
        {
            return $"{{{X},{Y}}}";
        }

        // 以下適当
        public static bool operator ==(Position p1, Position p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }
        public static bool operator !=(Position p1, Position p2)
        {
            return p1.X != p2.X || p1.Y != p2.Y;
        }

        public override bool Equals(object obj)
        {
            return (obj is Position p && this == p);
        }
        public override int GetHashCode()
        {
            return Y;
        }
    }
}
