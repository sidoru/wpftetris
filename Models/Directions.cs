﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTetris.Models
{
    public enum MoveDirection
    {
        Left,
        Right,
        Down,
    }

    public enum RotateDirection
    {
        Left,
        Right,
    }
}
