﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTetris.Models
{
    /// <summary>
    /// テトリミノ積み上げる場所
    /// </summary>
    public class FieldModel : ModelBase
    {
        /// <summary>
        /// enumの名前が同じTetriminoTypeからCellType取るためのDictionary
        /// </summary>
        private static readonly IDictionary<TetriminoType, CellType> CellTypeLookup =
            (from t in Enum.GetValues(typeof(TetriminoType)).Cast<TetriminoType>()
             from c in Enum.GetValues(typeof(CellType)).Cast<CellType>()
             where Enum.GetName(typeof(TetriminoType), t) == Enum.GetName(typeof(CellType), c)
             select new { t, c }).ToDictionary(r => r.t, r => r.c);

        public FieldModel()
        {
            Cells = new ReadOnlyDictionary<Position, CellModel>(CellMatrix.SelectMany(r => r).ToDictionary(c => c.Position));
        }

        /// <summary>
        /// テトリミノ配置空間
        /// 幅10高さ20の長方形
        /// </summary>
        public IReadOnlyList<List<CellModel>> CellMatrix { get; } = Enumerable.Range(0, 20).Select(y =>
                                                                    Enumerable.Range(0, 10).Select(x => new CellModel(x, y)).ToList()).ToList().AsReadOnly();

        /// <summary>
        /// テトリミノ配置空間
        /// 行列だと面倒な時もあるので座標で当たれるようにしたもので中身は一緒
        /// </summary>
        public IReadOnlyDictionary<Position, CellModel> Cells { get; }

        /// <summary>
        /// 今操作中のテトリミノ
        /// 場所や形変えるたびにインスタンス入替える
        /// </summary>
        public TetriminoModel CurrentTetrimino { get; private set; }

        /// <summary>
        /// 動かす
        /// </summary>
        /// <returns>動かせたらtrue</returns>
        public bool Move(MoveDirection direction)
        {
            var newTetrimino = CurrentTetrimino.Move(direction);
            if (IsPlaceable(newTetrimino))
            {
                Reposition(newTetrimino);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 回転する
        /// </summary>
        /// <returns>回転出来たらtrue</returns>
        public bool Rotate(RotateDirection direction)
        {
            var newTetrimino = CurrentTetrimino.Rotate(direction);
            if (IsPlaceable(newTetrimino))
            {
                Reposition(newTetrimino);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 動かして他と当たらないかチェック
        /// </summary>
        public bool IsPlaceable(TetriminoModel tetrimino)
        {
            foreach (var tp in tetrimino.FillPositions)
            {
                // セルがない、セルが埋まっているし自分自身以外
                if (!Cells.TryGetValue(tp, out var cell)
                    || (cell.CellType != CellType.None && CurrentTetrimino != null && !CurrentTetrimino.FillPositions.Any(p => p == cell.Position)))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// テトリミノ追加
        /// </summary>
        public void Add(TetriminoModel tetrimino)
        {
            CurrentTetrimino = tetrimino;
            Fill(CurrentTetrimino.FillPositions, CellTypeLookup[CurrentTetrimino.Type]);
        }
        /// <summary>
        /// 埋まった行インデックス取得
        /// </summary>
        public IList<int> GetLineRow()
        {
            var list = new List<int>();
            for (int y = 0; y < CellMatrix.Count; y++)
            {
                if (CellMatrix[y].All(c => c.CellType != CellType.None))
                {
                    list.Add(y);
                }
            }

            return list;
        }

        /// <summary>
        /// 行を消す
        /// </summary>
        public void DeleteRow(IList<int> rowIndexes)
        {
            foreach (var y in rowIndexes)
            {
                foreach (var cell in CellMatrix[y])
                {
                    cell.CellType = CellType.None;
                }
            }
        }

        /// <summary>
        /// 消した行を詰める
        /// </summary>
        public void ChinkRow(IList<int> deletedIndexes)
        {
            // 削除行を無視しながら下から順に上のセルコピーしていく
            var chinkIndex = CellMatrix.Count - 1;
            for (int y = CellMatrix.Count - 1; y >= 0; y--)
            {
                if (deletedIndexes.Contains(y))
                {
                    continue;
                }

                // 行コピー
                for (int x = 0; x < CellMatrix[chinkIndex].Count; x++)
                {
                    CellMatrix[chinkIndex][x].CellType = CellMatrix[y][x].CellType;
                }

                chinkIndex--;
            }
        }

        /// <summary>
        /// 操作中のテトリミノ再配置
        /// </summary>
        private void Reposition(TetriminoModel tetrimino)
        {
            if (CurrentTetrimino != null)
            {
                Fill(CurrentTetrimino.FillPositions, CellType.None);
            }

            Add(tetrimino);
        }

        /// <summary>
        /// 指定された場所をセルの種類で埋める
        /// </summary>
        private void Fill(IList<Position> positions, CellType cellType)
        {
            foreach (var pos in positions)
            {
                Cells[pos].CellType = cellType;
            }
        }

        /// <summary>
        /// ToString
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder();

            foreach (var row in CellMatrix)
            {
                foreach (var cell in row)
                {
                    sb.Append((cell.CellType != CellType.None) ? "■" : "□");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
