﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTetris.Models
{
    /// <summary>
    /// メイン
    /// </summary>
    public class MainModel : ModelBase
    {
        /// <summary>
        /// テトリミノ種別リスト
        /// </summary>
        private static readonly IList<TetriminoType> TetriminoTypes = Enum.GetValues(typeof(TetriminoType)).Cast<TetriminoType>().ToList();

        /// <summary>
        /// テトリミノ生成用乱数
        /// </summary>
        private readonly Random random = new Random();

        /// <summary>
        /// テトリミノが表示されてるフィールド
        /// </summary>
        public FieldModel Field { get; set; } = new FieldModel();

        /// <summary>
        /// テトリミノ屋さん
        /// </summary>
        public TetriminoFactory TetriminoFactory { get; } = new TetriminoFactory();

        /// <summary>
        /// メインタイマー
        /// </summary>
        public IDisposable MainTimer { get; private set; }

        public void Start()
        {
            PushNewTetrimino();
            MainTimer?.Dispose();
            MainTimer = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(i => Main());
        }

        public async void Main()
        {
            if (!Field.Move(MoveDirection.Down))
            {
                var lines = Field.GetLineRow();
                if (lines.Any())
                {
                    // ちょっと止めて消えてる感演出してから行詰める
                    Field.DeleteRow(lines);
                    await Task.Delay(200);
                    Field.ChinkRow(lines);
                }

                PushNewTetrimino();
            }
        }

        public void MoveLeft()
        {
            Field.Move(MoveDirection.Left);
        }

        public void MoveRight()
        {
            Field.Move(MoveDirection.Right);
        }

        public void MoveDown()
        {
            Field.Move(MoveDirection.Down);
        }

        public void RotateLeft()
        {
            Field.Rotate(RotateDirection.Left);
        }

        public void RotateRight()
        {
            Field.Rotate(RotateDirection.Left);
        }

        public void PushNewTetrimino()
        {
            var type = TetriminoTypes[random.Next(TetriminoTypes.Count - 1)];
            var tm = TetriminoFactory.Create(type, new Position(2, 0));
            Field.Add(tm);
        }
    }
}
