﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfTetris.Models
{
    /// <summary>
    /// テトリミノ
    /// </summary>
    public class TetriminoModel : ModelBase
    {
        // ctor
        public TetriminoModel(TetriminoType type, byte[,] shape, Position position)
        {
            Type = type;
            Shape = shape;
            Position = position;
            FillPositions = GetFillPositions();
        }

        /// <summary>
        /// 形のビット表現
        /// 座標を回転すんのが数学だったのでそのまま保持
        /// </summary>
        public byte[,] Shape { get; }

        /// <summary>
        /// Field上の座標
        /// </summary>
        public Position Position { get; }

        /// <summary>
        /// テトリミノ種別
        /// </summary>
        public TetriminoType Type { get; }

        /// <summary>
        /// 有効座標リスト
        /// </summary>
        public IList<Position> FillPositions { get; }

        /// <summary>
        /// 横移動する
        /// </summary>
        /// <param name="position">場所</param>
        public TetriminoModel Move(Position newPosition)
        {
            return new TetriminoModel(Type, Shape, newPosition);
        }

        /// <summary>
        /// 横移動する
        /// </summary>
        /// <param name="position">場所</param>
        public TetriminoModel Move(int x, int y)
        {
            return Move(new Position(x, y));
        }

        /// <summary>
        /// 移動する
        /// </summary>
        /// <param name="direction">方向</param>
        public TetriminoModel Move(MoveDirection direction)
        {
            Position newPosition;
            if (direction == MoveDirection.Left)
            {
                newPosition = new Position(Position.X - 1, Position.Y);
            }
            else if (direction == MoveDirection.Right)
            {
                newPosition = new Position(Position.X + 1, Position.Y);
            }
            else
            {
                newPosition = new Position(Position.X, Position.Y + 1);
            }

            return Move(newPosition);
        }

        /// <summary>
        /// 回転する
        /// </summary>
        /// <param name="direction"></param>
        public TetriminoModel Rotate(RotateDirection direction)
        {
            // 縦横入替えた配列
            var width = Shape.GetLength(0);
            var height = Shape.GetLength(1);
            var newShape = new byte[height, width];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (direction == RotateDirection.Left)
                    {
                        newShape[height - y - 1, x] = Shape[x, y];
                    }
                    else
                    {
                        newShape[y, width - x - 1] = Shape[x, y];
                    }
                }
            }

            return new TetriminoModel(Type, newShape, Position);
        }

        /// <summary>
        /// 指定された位置・形の埋める座標取得
        /// </summary>
        private IList<Position> GetFillPositions()
        {
            var width = Shape.GetLength(0);
            var height = Shape.GetLength(1);

            var positions = new List<Position>();
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (Shape[x, y] == 1)
                    {
                        positions.Add(new Position(x + Position.X, y + Position.Y));
                    }
                }
            }

            return positions;
        }

        /// <summary>
        /// ToString
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder();

            for (int x = 0; x < Shape.GetLength(0); x++)
            {
                for (int y = 0; y < Shape.GetLength(1); y++)
                {
                    sb.Append((Shape[x, y] == 1) ? "■" : "□");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
